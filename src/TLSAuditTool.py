#!/usr/bin/env python3

import base64
from dataclasses import dataclass
from datetime import datetime, timezone
import hashlib
import json
from OpenSSL.crypto import dump_publickey, FILETYPE_ASN1, load_certificate # pip install pyopenssl
import re
import socket
import ssl

class B32C:
    """
The code in this class is based on https://github.com/ingydotnet/crockford-py

Copyright (c) 2011, Ingy dot Net
All rights reserved.

Redistribution and use in source and binary forms, with or
without modification, are permitted provided that the following
conditions are met:

1) Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2) Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY Ingy dot Net AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Ingy dot Net OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation
are those of the authors and should not be interpreted as representing
official policies, either expressed or implied, of Ingy dot Net.
    """

    __std2crock = str.maketrans(
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567",
        "0123456789ABCDEFGHJKMNPQRSTVWXYZ",
        '='
    )
    __crock2std = str.maketrans(
        "0123456789ABCDEFGHJKMNPQRSTVWXYZOIL",
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567ABB",
        '='
    )

    def b32encode(s):
        return base64.b32encode(s).decode().translate(B32C.__std2crock)

    def b32decode(b32, casefold=None, map01=None):
        # Ensure the manatory padding is correct:
        b32 += '=' * ((8 - len(b32) % 8) % 8)
        return base64.b32decode(b32.upper().translate(B32C.__crock2std),
            casefold=casefold, map01=map01)

class Luhn:
    def luhnModN(s: str, alphabet: str) -> str:
        """
        returns the Luhn mod N check digit of a string and a given alphabet
        """
        count = 0
        luhnVal = 0
        multiplyOn = (len(s) + 1) % 2
        alphabetLength = len(alphabet)
        for char in s:
            i = alphabet.find(char)

            if count % 2 == multiplyOn:
                i = i * 2

            i = int(i / alphabetLength) + (i % alphabetLength)
            luhnVal = (luhnVal + i) % alphabetLength
            count = count + 1
        luhnVal = (alphabetLength - (luhnVal % alphabetLength)) % alphabetLength
        return alphabet[luhnVal]

class CB32Code:
    def fromInt(i: int, separator: str = '-') -> str:
        if not isinstance(i, int):
            raise TypeError('i must be int')
        b = i.to_bytes(length=(((max(i.bit_length(), 1) + 7) // 8) // 5 + 1) * 5, byteorder='big')
        bs = B32C.b32encode(b)
        c = Luhn.luhnModN(bs, '0123456789ABCDEFGHJKMNPQRSTVWXYZ')
        code = separator.join(bs[x:x+4] for x in range(0, len(bs), 4)) + separator + c
        return re.sub(r'^(0000' + re.escape(separator) + ')+', '', code)
    
    def toInt(s: str, separator: str = '-') -> int:
        if not isinstance(s, str):
            raise TypeError('s must be str')
        string = s.replace(separator, '').upper()
        toBeChecked = string[:-1]
        actualCheckDigit = string[-1:]
        expectedCheckDigit = Luhn.luhnModN(toBeChecked, '0123456789ABCDEFGHJKMNPQRSTVWXYZ')
        if actualCheckDigit != expectedCheckDigit:
            raise ValueError(f'check digit does not match: found {actualCheckDigit} but expected {expectedCheckDigit} in {string}')
        padding = '0' * ((8 - len(toBeChecked) % 8) % 8)
        return int(B32C.b32decode(padding + toBeChecked).hex(), 16)

@dataclass
class TlsAuditScan:
    hostname: str
    port: int
    cert: bytes
    certFpRaw: bytes
    certFp: str
    pubkeyFpRaw: bytes
    pubkeyFp: str
    notBefore: datetime
    notAfter: datetime
    serialRaw: bytes
    serial: str
    subject: dict
    issuer: dict
    scanTimestamp: datetime
    scanIdInt: int = 0

    def print(self, compareTo = None, summary: bool = False) -> None:
        if compareTo is not None and not isinstance(compareTo, TlsAuditScan):
            raise TypeError("'compareTo must be an instance of 'TlsAuditScan' or None")
        if not summary or (compareTo is not None and (compareTo.pubkeyFp != self.pubkeyFp or compareTo.certFp != self.certFp)):
            print('scan ID       :', CB32Code.fromInt(self.scanIdInt), '(comparing to ' + CB32Code.fromInt(compareTo.scanIdInt) + ')' if compareTo is not None else '')
            print('scan timestamp:', self.scanTimestamp.isoformat(), '(comparing to ' + compareTo.scanTimestamp.isoformat() + ')' if compareTo is not None else '')
            print('hostname      :', self.hostname, '(comparing to ' + compareTo.hostname + ')' if compareTo is not None and compareTo.hostname != self.hostname else '')
            print('port          :', self.port, '(comparing to ' + str(compareTo.port) + ')' if compareTo is not None and compareTo.port != self.port else '')
            print('pubkey fp     :', self.pubkeyFp, '(does not match ' + compareTo.pubkeyFp + ')' if compareTo is not None and compareTo.pubkeyFp != self.pubkeyFp else '(matches)' if compareTo is not None else '')
            print('cert fp       :', self.certFp, '(does not match ' + compareTo.certFp + ')' if compareTo is not None and compareTo.certFp != self.certFp else '(matches)' if compareTo is not None else '')
            print('cert subject  :', json.dumps(self.subject), '(does not match ' + json.dumps(compareTo.subject) + ')' if compareTo is not None and json.dumps(compareTo.subject) != json.dumps(self.subject) else '(matches)' if compareTo is not None else '')
            print('cert issuer   :', json.dumps(self.issuer), '(does not match ' + json.dumps(compareTo.issuer) + ')' if compareTo is not None and json.dumps(compareTo.issuer) != json.dumps(self.issuer) else '(matches)' if compareTo is not None else '')
            print('cert notBefore:', self.notBefore.isoformat(), '(does not match ' + compareTo.notBefore.isoformat() + ')' if compareTo is not None and compareTo.notBefore.isoformat() != self.notBefore.isoformat() else '(matches)' if compareTo is not None else '')
            print('cert notAfter :', self.notAfter.isoformat(), '(does not match ' + compareTo.notAfter.isoformat() + ')' if compareTo is not None and compareTo.notAfter.isoformat() != self.notAfter.isoformat() else '(matches)' if compareTo is not None else '')
            print('cert serial   :', self.serial, '(does not match ' + compareTo.serial + ')' if compareTo is not None and compareTo.serial != self.serial else '(matches)' if compareTo is not None else '')


class TlsAuditTool(object):

    def __init__(self, database: str):
        self.__init_db(database)

    def scan(self, hostname: str, port: int):
        hostname = hostname
        port = port
        cert = TlsAuditTool.__fetchDerCert(hostname, port)
        pubkey = TlsAuditTool.__extractPubkeyFromDerCert(cert)
        certFpRaw = TlsAuditTool.__getFingerprint(cert)
        certFp = TlsAuditTool.__b64(certFpRaw)
        pubkeyFpRaw = TlsAuditTool.__getFingerprint(pubkey)
        pubkeyFp = TlsAuditTool.__b64(pubkeyFpRaw)
        #certDetails = TlsAuditTool.__extractDataFromDerCert(cert)
        opensslCert = load_certificate(FILETYPE_ASN1, cert)
        notBefore = datetime.strptime(opensslCert.get_notBefore().decode(), '%Y%m%d%H%M%S%z')
        notAfter = datetime.strptime(opensslCert.get_notAfter().decode(), '%Y%m%d%H%M%S%z')
        serialInt = opensslCert.get_serial_number()
        serialRaw = serialInt.to_bytes(length=(max(serialInt.bit_length(), 1) + 7) // 8, byteorder='big')
        serial = serialRaw.hex()
        subject = {x[0].decode(): x[1].decode() for x in opensslCert.get_subject().get_components()}
        issuer = {x[0].decode(): x[1].decode() for x in opensslCert.get_issuer().get_components()}
        scanTimestamp = datetime.now(timezone.utc)
        self._scan = TlsAuditScan(
            hostname, 
            port, 
            cert, 
            certFpRaw, 
            certFp, 
            pubkeyFpRaw, 
            pubkeyFp, 
            notBefore.astimezone(), 
            notAfter.astimezone(), 
            serialRaw, 
            serial, 
            subject, 
            issuer, 
            scanTimestamp.astimezone()
        )
        self.__save_scan()
    
    def getLatestScan(self, hostname: str, port: int = 0, skip: int = 0):
        sql = '''
            SELECT 
                `l`.`hostname`, 
                `l`.`port`, 
                `c`.`rawCert`,
                `l`.`certFingerprint`, 
                `c`.`pubkeyFingerprint`, 
                `c`.`notBefore`, 
                `c`.`notAfter`, 
                `c`.`serial`,
                `c`.`subject`, 
                `c`.`issuer`, 
                `l`.`timestamp`, 
                `l`.`_rowid_`
            FROM `log` AS `l` 
            LEFT OUTER JOIN `certificate` AS `c` ON `l`.`certFingerprint` = `c`.`certFingerprint` 
            WHERE `hostname` = ? 
        '''
        if type(port) == int and port > 0:
            sql = sql + 'AND `port` = ? '
        sql = sql + '''
            ORDER BY `l`.`timestamp` DESC 
            LIMIT 1 OFFSET ?;
        '''

        sqlData = (hostname, skip, )
        if type(port) == int and port > 0:
            sqlData = (hostname, port, skip, )

        res = self.__cur.execute(sql, sqlData).fetchone()

        if res is None:
            return None

        return TlsAuditScan(
            res[0], # host
            res[1], # port
            res[2], # cert
            res[3], # certFpRaw
            base64.b64encode(res[3]).decode(), # certFp
            res[4], # pubkeyFpRaw
            base64.b64encode(res[4]).decode(), # pubkeyFp
            datetime.fromisoformat(res[5]).astimezone(), # notBefore
            datetime.fromisoformat(res[6]).astimezone(), # notAfter
            res[7], # serialRaw
            res[7].hex(), # serial
            json.loads(res[8]), # subject
            json.loads(res[9]), # issuer
            datetime.fromisoformat(res[10]).astimezone(), # scanTimestamp
            res[11] # scanIdInt
        )

    def getScanById(self, id: int):
        sql = '''
            SELECT 
                `l`.`hostname`, 
                `l`.`port`, 
                `c`.`rawCert`,
                `l`.`certFingerprint`, 
                `c`.`pubkeyFingerprint`, 
                `c`.`notBefore`, 
                `c`.`notAfter`, 
                `c`.`serial`,
                `c`.`subject`, 
                `c`.`issuer`, 
                `l`.`timestamp`, 
                `l`.`_rowid_`
            FROM `log` AS `l` 
            LEFT OUTER JOIN `certificate` AS `c` ON `l`.`certFingerprint` = `c`.`certFingerprint` 
            WHERE `l`.`_rowid_` = ?;
        '''

        sqlData = (id, )

        res = self.__cur.execute(sql, sqlData).fetchone()

        if res is None:
            return None

        return TlsAuditScan(
            res[0], # host
            res[1], # port
            res[2], # cert
            res[3], # certFpRaw
            base64.b64encode(res[3]).decode(), # certFp
            res[4], # pubkeyFpRaw
            base64.b64encode(res[4]).decode(), # pubkeyFp
            datetime.fromisoformat(res[5]).astimezone(), # notBefore
            datetime.fromisoformat(res[6]).astimezone(), # notAfter
            res[7], # serialRaw
            res[7].hex(), # serial
            json.loads(res[8]), # subject
            json.loads(res[9]), # issuer
            datetime.fromisoformat(res[10]).astimezone(), # scanTimestamp
            res[11] # scanIdInt
        )

    def getKnownHosts(self):
        sql = '''
            SELECT 
                DISTINCT `hostname`
            FROM `log`
            ORDER BY `hostname` ASC;
        '''

        res = self.__cur.execute(sql).fetchall()

        if res is None:
            return None

        return map(lambda x: x[0], res)

    def getKnownHostPorts(self):
        sql = '''
            SELECT `hostname`, `port`
            FROM `log` 
            GROUP BY `hostname`, `port` 
            ORDER BY `hostname`, `port`;
        '''

        res = self.__cur.execute(sql).fetchall()

        if res is None:
            return None

        return map(lambda x: {'host': x[0], 'port': x[1]}, res)

    def __init_db(self, database: str) -> None:
        self.__con = sqlite3.connect(database)
        self.__cur = self.__con.cursor()
        self.__cur.execute('''
            CREATE TABLE IF NOT EXISTS `certificate` (
                `certFingerprint` BLOB PRIMARY KEY, 
                `pubkeyFingerprint` BLOB NOT NULL,
                `serial` BLOB NOT NULL,
                `notBefore` TEXT NULL,
                `notAfter` TEXT NULL,
                `subject` TEXT NULL,
                `issuer` TEXT NULL,
                `rawCert` BLOB NOT NULL
            );
        ''')
        self.__cur.execute('''
            CREATE TABLE IF NOT EXISTS `log` (
                `hostname` TEXT NOT NULL,
                `port` INT NOT NULL,
                `certFingerprint` BLOB, 
                `timestamp` TEXT NOT NULL
            );
        ''')
    
    def __save_scan(self) -> None:
        self.__cur.execute('''
            INSERT OR IGNORE INTO `certificate` (
                `certFingerprint`,
                `pubkeyFingerprint`,
                `serial`,
                `notBefore`,
                `notAfter`,
                `subject`,
                `issuer`,
                `rawCert`
            ) VALUES (
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?
            );
        ''', (
            self._scan.certFpRaw, 
            self._scan.pubkeyFpRaw, 
            self._scan.serialRaw,
            self._scan.notBefore.astimezone(timezone.utc).isoformat(),
            self._scan.notAfter.astimezone(timezone.utc).isoformat(),
            json.dumps(self._scan.subject),
            json.dumps(self._scan.issuer),
            self._scan.cert
            )
        )

        self.__cur.execute('''
            INSERT INTO `log` (
                `hostname`,
                `port`,
                `certFingerprint`,
                `timestamp`
            ) VALUES (
                ?,
                ?,
                ?,
                ?
            );
        ''', (
            self._scan.hostname,
            self._scan.port,
            self._scan.certFpRaw, 
            self._scan.scanTimestamp.astimezone(timezone.utc).isoformat()
            )
        )

        self.__con.commit()
        self._scan.scanIdInt = self.__cur.lastrowid

    def __fetchDerCert(hostname: str, port: int) -> bytes:
        context = ssl.create_default_context()
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        conn = context.wrap_socket(s, server_side=False, server_hostname=hostname)
        conn.settimeout(5)
        conn.connect((hostname, port))
        derCert = conn.getpeercert(binary_form=True)
        conn.close()
        return derCert

    def __extractPubkeyFromDerCert(derCert: bytes) -> bytes:
        return dump_publickey(FILETYPE_ASN1, load_certificate(FILETYPE_ASN1, derCert).get_pubkey())
    
    def __extractDataFromDerCert(derCert: bytes) -> dict:
        cert = load_certificate(FILETYPE_ASN1, derCert)
        notBefore = datetime.strptime(cert.get_notBefore().decode(), '%Y%m%d%H%M%S%z')
        #notBefore.tzinfo = timezone.utc
        notAfter = datetime.strptime(cert.get_notAfter().decode(), '%Y%m%d%H%M%S%z')
        #notAfter.tzinfo = timezone.utc
        return {
            'notBefore': notBefore,
            'notAfter': notAfter,
            'serial': cert.get_serial_number(),
            'issuer': cert.get_issuer(),
            'subject': cert.get_subject(),
        }
    
    def __getFingerprint(b: bytes) -> bytes:
        return hashlib.sha256(b).digest()
    
    def __b64(b: bytes) -> str:
        return base64.b64encode(b).decode()
    


if __name__ == '__main__':

    import argparse, json, sqlite3, sys, os

    parser = argparse.ArgumentParser()
    parser.add_argument("command", choices=('compare', 'hostports', 'hosts', 'last', 'scan', 'scanall'), type=str, help="Command")
    parser.add_argument('-n', "--host", type=str, help='The hostname to get the certificate from')
    parser.add_argument('-p', "--port", type=int, help='The port number. Optional. Default: 443', default=443)
    parser.add_argument('-s', "--skip", type=int, help='Number of scans to skip when using \'last\' command. Optional. Default: 0', default=0)
    parser.add_argument('-i', "--scan-id", type=str, help='ID of the scan to show when using \'compare\' or \'last\' command. Optional.', default=None)
    parser.add_argument("--to-host", type=str, help='Host to compare to when using \'compare\' command', default=None)
    parser.add_argument("--to-port", type=int, help='Port to compare to when using \'compare\' command', default=None)
    parser.add_argument("--to-scan-id", type=str, help='ID of the scan to compare to when using \'compare\' command', default=None)
    parser.add_argument("-d", "--database", type=str, help='The database file to use. Overrides the value in environment variable TLSAT_DATABASE')
    args = parser.parse_args()

    database = None
    if args.database is not None and args.database != '':
        database = args.database
    elif 'TLSAT_DATABASE' in os.environ and os.environ['TLSAT_DATABASE'] is not None and os.environ['TLSAT_DATABASE'] != '':
        database = os.environ['TLSAT_DATABASE']
    else:
        print("Please specify the database file via the '--database' command line argument or the TLSAT_DATABASE environment variable.", file=sys.stderr)
        exit(1)

    
    t = TlsAuditTool(database)

    if args.command == 'scan':
        if args.host is None:
            print('Missing hostname', file=sys.stderr)
            exit(1)
        lastScan = t.getLatestScan(args.host, args.port)
        t.scan(args.host, args.port)
        thisScan = t._scan
        thisScan.print(lastScan)
    elif args.command == 'last':
        if args.host is None and args.scan_id is None:
            print('Missing hostname or scan ID', file=sys.stderr)
            exit(1)
        lastScan = None
        secondToLastScan = None
        if args.scan_id is not None:
            lastScan = t.getScanById(CB32Code.toInt(args.scan_id))
            if args.to_scan_id is not None:
                secondToLastScan = t.getScanById(CB32Code.toInt(args.to_scan_id))
        else:
            lastScan = t.getLatestScan(args.host, args.port, args.skip)
            if args.to_scan_id is not None:
                secondToLastScan = t.getScanById(CB32Code.toInt(args.to_scan_id))
            else:
                secondToLastScan = t.getLatestScan(args.host, args.port, args.skip +1)
        lastScan.print(secondToLastScan) if lastScan is not None else print("No scan found for ID '" + args.scan_id + "'") if args.scan_id is not None else print("No scan found for host '" + args.host + "' and port " + str(args.port))
    elif args.command == 'compare':
        if args.host is None and args.scan_id is None:
            print('Missing hostname or scan ID', file=sys.stderr)
            exit(1)
        compareToHost = args.to_host if args.to_host is not None else args.host
        compareToPort = args.to_port if args.to_port is not None else args.port
        theScan = None
        theOtherScan = None
        if args.scan_id is not None:
            theScan = t.getScanById(CB32Code.toInt(args.scan_id))
        else:
            theScan = t.getLatestScan(args.host, args.port, args.skip)
        if args.to_scan_id is not None:
            theOtherScan = t.getScanById(CB32Code.toInt(args.to_scan_id))
        else:
            theOtherScan = t.getLatestScan(compareToHost, compareToPort, args.skip)
        theScan.print(theOtherScan) if theScan is not None else print("No scan found for ID '" + args.scan_id + "'") if args.scan_id is not None else print("No scan found for host '" + args.host + "' and port " + str(args.port))
    elif args.command == 'hosts':
        for h in t.getKnownHosts():
            print(h)
    elif args.command == 'hostports':
        for h in t.getKnownHostPorts():
            print(h['host'], h['port'])
    elif args.command == 'scanall':
        for host in t.getKnownHostPorts():
            print('>>> Scanning ' + host['host'] + ':' + str(host['port']))
            lastScan = t.getLatestScan(host['host'], host['port'])
            try:
                t.scan(host['host'], host['port'])
                thisScan = t._scan
                thisScan.print(lastScan, summary=True)
            except BaseException as e:
                print('Error:', e)
